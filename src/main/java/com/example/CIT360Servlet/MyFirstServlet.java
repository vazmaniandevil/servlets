package com.example.CIT360Servlet;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "MyFirstServlet", value = "/MyFirstServlet")
public class MyFirstServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("This page is not available, please go back to the previous page.");
        out.println("<html><head></head><body>");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try
        {
            PrintWriter out = response.getWriter();
            response.setContentType("text/html");
            out.println("<html><head></head><body>");
            String make = request.getParameter("make");
            String model = request.getParameter("model");
            String year = request.getParameter("year");
            out.println("<h1>Your favorite car!</h1>");
            out.println("<p>Make: " + make + "</p>");
            out.println("<p>Model: " + model + "</p>");
            out.println("<p>Year: " + year + "</p>");
            out.println("</body></html>");
        }
        catch (IOException e)
        {
            System.out.println("There was a problem, please, try again!");
        }

    }
}
